﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Twitter;
using Owin;

namespace SimpleSocialLogin
{
    public class Startup
    {
        public static void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ThisCanBeAnythingYouLike",
                LoginPath = new PathString("/Auth/Login")
            });

            appBuilder.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AppId = "1001912416547044",
                AppSecret = "ffc53b2bc1f2173f9a1d463ec5c8146d",
                SignInAsAuthenticationType = "ThisCanBeAnythingYouLike",
            });

            appBuilder.UseTwitterAuthentication(new TwitterAuthenticationOptions
            {
                ConsumerKey = "QL71O7r6AQ98apbFwjagQF9Y7",
                ConsumerSecret = "vcXULk32MyThfgq60ZjT9HCN55wNyvj7li451kFLKPtfcgCig1",
                SignInAsAuthenticationType = "ThisCanBeAnythingYouLike",
                BackchannelCertificateValidator = null   // can be for demo purposes
            });


            appBuilder.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                ClientId = "1092625625508-bl6rvttck0qb1n02mnjti9ph5scm609r.apps.googleusercontent.com",
                ClientSecret = "eoAieOkoj7RbsfZYs5UtdurQ",
                SignInAsAuthenticationType = "ThisCanBeAnythingYouLike",
            });
        }
    }
}