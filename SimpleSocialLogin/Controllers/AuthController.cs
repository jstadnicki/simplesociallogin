﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using SimpleSocialLogin.Models;

namespace SimpleSocialLogin.Controllers
{
    public class AuthController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            var model = new LoginModel();

            var providers =
                HttpContext
                    .GetOwinContext()
                    .Authentication
                    .GetAuthenticationTypes(x => !string.IsNullOrWhiteSpace(x.Caption))
                    .ToList();

            model.Providers = providers;

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (model.Username.Equals("jaroslaw", StringComparison.OrdinalIgnoreCase) &&
                model.Password.Equals("stadnicki", StringComparison.OrdinalIgnoreCase))
            {
                var identity = new ClaimsIdentity("ThisCanBeAnythingYouLike"); // the same as in startup
                identity.AddClaims(new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, model.Username),
                    new Claim(ClaimTypes.Name, model.Username)
                });

                HttpContext.GetOwinContext().Authentication.SignIn(identity);
            }
            return View();
        }

        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult SocialLogin(string id)
        {
            HttpContext.GetOwinContext().Authentication.Challenge(
              new AuthenticationProperties
              {
                  RedirectUri = "/secret"
              },
              id);
            return new HttpUnauthorizedResult();

        }
    }
}